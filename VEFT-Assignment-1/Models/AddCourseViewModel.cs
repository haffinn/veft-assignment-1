﻿using System;
using System.ComponentModel.DataAnnotations;


namespace VEFT_Assignment_1.Models
{
    /// <summary>
    /// This is a model class to be used when creating a new instance of a course.
    /// Does not allow users to specify ID of the course.
    /// </summary>
    public class AddCourseViewModel
    {
        /// <summary>
        /// Name of the course. 
        /// Example: "Web services"
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// An identifier for the template of the course.
        /// Example: "T-514-VEFT"
        /// </summary>
        [Required]
        public string TemplateId { get; set; }
        /// <summary>
        /// A date for when the class begins.
        /// Example: "2015-08-17"
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// A date for when the class ends.
        /// Example: "2015-11-08"
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }
    }
}