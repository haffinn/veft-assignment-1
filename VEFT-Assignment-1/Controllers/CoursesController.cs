﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using VEFT_Assignment_1.Models;

namespace VEFT_Assignment_1.Controllers
{
    /// <summary>
    /// This class manages all functions related to courses and course instances
    /// </summary>
    [RoutePrefix("api/courses")]
    public class CoursesController : ApiController
    {
        private static List<Course> _courses;
        
        /// <summary>
        /// A constructor (with some example data)
        /// </summary>
        public CoursesController()
        {
            if (_courses == null)
            {
                _courses = new List<Course>
                {
                    new Course
                    {
                        Id = 1,
                        Name = "Web services",
                        TemplateId = "T-514-VEFT",
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddMonths(3),
                        StudentsInClass = new List<Student>
                        {
                            new Student
                            {
                                Name = "Hafþór Snær Þórsson",
                                SSN = "2204922819"
                            },
                            new Student
                            {
                                Name = "Kári Mímisson",
                                SSN = "1711922859"
                            }
                        }
                    },
                    new Course
                    {
                        Id = 2,
                        Name = "Web programming II",
                        TemplateId = "T-427-WEPO",
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddMonths(3),
                        StudentsInClass = new List<Student>
                        {
                            new Student
                            {
                                Name = "Daníel Brandur Sigurgeirsson",
                                SSN = "1203735289"
                            },
                            new Student
                            {
                                Name = "Freysteinn Alfreðsson",
                                SSN = "1807825919"
                            },
                            new Student
                            {
                                Name = "Ýmir Vigfússon",
                                SSN = "1301848209"
                            }
                        }

                    },
                    new Course
                    {
                        Id = 3,
                        Name = "Computer security",
                        TemplateId = "T-417-TOOR",
                        StartDate = DateTime.Now,
                        EndDate = DateTime.Now.AddMonths(3),
                        StudentsInClass = new List<Student>
                        {
                            new Student
                            {
                                Name = "Guðjón Geir Jónsson	",
                                SSN = "0903912089"
                            },
                            new Student
                            {
                                Name = "Hafþór Snær Þórsson",
                                SSN = "2204922819"
                            },
                            new Student
                            {
                                Name = "Ragnar Þór Valgeirsson",
                                SSN = "1501922189"
                            }
                            
                        }
                    }
                };
            }
        }

        /// <summary>
        /// Get list of all courses
        /// </summary>
        /// <returns>List of all courses</returns>
        [HttpGet]
        [Route("")]
        public List<Course> GetCourses()
        {
            return _courses;
        }

        /// <summary>
        /// Get a single course by a given ID
        /// </summary>
        /// <param name="id">Enter a ID of a course</param>
        /// <returns>A course instance</returns>
        [HttpGet]
        [Route("{id}", Name = "GetCourse")]
        [ResponseType(typeof(Course))]
        public IHttpActionResult GetCoursebyId(int id)
        {
            if (id.GetType() != typeof(int))
            {
                return BadRequest();
            }
            Course thiscourse = _courses.Find(x => x.Id == id);
            if (thiscourse == null)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, thiscourse);
        }

        /// <summary>
        /// Adds a new course to the "database"
        /// </summary>
        /// <param name="model">Name, templateID, start and end date</param>
        /// <returns>Returns a location to the new course</returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostNewCourse(AddCourseViewModel model)
        {
            if (model != null)
            {
                if (model.Name == null ||
                    model.TemplateId == null)
                {
                    return BadRequest("One or more values are missing or not specified");
                }

                // Random error checker - if no date is specified, it gets default values NOW and start+3 months.
                // Would probably not be used in a real application... lol
                if (model.StartDate == DateTime.MinValue) model.StartDate = DateTime.Now;
                if (model.EndDate == DateTime.MinValue) model.EndDate = model.StartDate.AddMonths(3);

                Course newCourse = new Course();

                int newId;
                if (_courses == null)
                {
                    newId = 1;
                }
                else
                {
                    newId = _courses.Last().Id + 1; // Hack: Get ID of latest course in the list + 1
                    newCourse.Id = newId;
                }

                newCourse.Name = model.Name;
                newCourse.TemplateId = model.TemplateId;
                newCourse.StartDate = model.StartDate;
                newCourse.EndDate = model.EndDate;

                // ReSharper disable once PossibleNullReferenceException
                _courses.Add(newCourse);
                var location = Url.Link("GetCourse", new { id = newId });
                return Created(location, newCourse);
            }
            return BadRequest("Body can't be empty");
        }

        /// <summary>
        /// Delete a course
        /// </summary>
        /// <param name="id">ID of the course you want to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult DeleteCourse(int id)
        {
            if (id.GetType() != typeof(int))
            {
                return BadRequest();
            }

            Course thiscourse = _courses.Find(x => x.Id == id);

            if (thiscourse != null)
            {
                _courses.Remove(thiscourse);
                return StatusCode(HttpStatusCode.NoContent);
            }
            return NotFound();
        }

        /// <summary>
        /// Update a course instance
        /// </summary>
        /// <param name="id">ID of the course you want to update</param>
        /// <param name="model">CourseViewModel - info on the course</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult UpdateCourse(int id, AddCourseViewModel model)
        {
            if (model == null)
            {
                return BadRequest("Body can't be empty");
            }
            if (id.GetType() != typeof(int))
            {
                return BadRequest();
            }
            if (model.Name == null ||
                model.TemplateId == null)
            {
                return BadRequest("One or more values are missing or not specified");
            }

            // Random error checker - if no date is specified, it gets default values NOW and start+3 months.
            // Would probably not be used in a real application... lol
            if (model.StartDate == DateTime.MinValue) model.StartDate = DateTime.Now;
            if (model.EndDate == DateTime.MinValue) model.EndDate = model.StartDate.AddMonths(3);

            Course thiscourse = _courses.Find(x => x.Id == id);

            if (thiscourse != null)
            {
                thiscourse.Name = model.Name;
                thiscourse.TemplateId = model.TemplateId;
                thiscourse.StartDate = model.StartDate;
                thiscourse.EndDate = model.EndDate;

                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Get a list of all students in a given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}/students")]
        [ResponseType(typeof(List<Student>))]
        public IHttpActionResult GetStudentsInCourse(int id)
        {
            if (id.GetType() != typeof(int))
            {
                return BadRequest();
            }
            Course thisCourse = _courses.Find(x => x.Id == id);
            if (thisCourse == null)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, thisCourse.StudentsInClass );
        }


        /// <summary>
        /// Add a student to a course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info on the student. Required: SSN and Name</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{id}/students", Name="GetStudents")]
        public IHttpActionResult AddNewStudentToCourse(int id, Student model)
        {
            if (id.GetType() != typeof(int))
            {
                return BadRequest();
            }
            if (model == null)
            {
                return BadRequest("Body can't be empty");
            }
            if (model.SSN == null || model.Name == null)
            {
                return BadRequest("One or more values are missing or not specified");
            }

            Student newStudent = new Student();
            newStudent.SSN = model.SSN;
            newStudent.Name = model.Name;

            Course thisCourse = _courses.Find(x => x.Id == id);
            if (thisCourse == null)
            {
                return NotFound();
            }
            thisCourse.StudentsInClass.Add(newStudent);

            var location = Url.Link("GetStudents", new { id = thisCourse.Id });
            return Created(location, newStudent);
        }

    }
}
